import { Component, OnInit } from '@angular/core';
import {
  faBars,
  faChevronLeft,
  faChevronRight,
  faChevronUp,
  faHiking,
  faHome,
  faLightbulb,
  faNewspaper,
  faPen,
  faPlus,
  faQuestion,
  faSearch,
  faShare,
  faShoppingCart,
  faSignInAlt,
  faThumbsDown,
  faThumbsUp,
  faUsers,
} from '@fortawesome/free-solid-svg-icons';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GetAllCourses } from 'src/app/stores/home/actions';
import { CourseModel } from 'src/app/stores/home/models';
import { CourseState } from 'src/app/stores/home/states';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ApiService } from 'src/app/services/api/api.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  // fontawesome icons start
  faBars = faBars;
  faSignInAlt = faSignInAlt;
  faHome = faHome;
  faHiking = faHiking;
  faLightbulb = faLightbulb;
  faNewspaper = faNewspaper;
  faSearch = faSearch;
  faShoppingCart = faShoppingCart;
  faPlus = faPlus;
  faPen = faPen;
  faQuestion = faQuestion;
  faShare = faShare;
  faUsers = faUsers;
  faChevronLeft = faChevronLeft;
  faChevronRight = faChevronRight;
  faChevronUp = faChevronUp;
  faThumbsUp = faThumbsUp;
  faThumbsDown = faThumbsDown;
  // fontawesome icons end

  helper = new JwtHelperService();
  courseList!: CourseModel[];
  studiedCourseList!: CourseModel[];
  isTokenExpired: boolean;
  @Select(CourseState.getCourseState) courses$: Observable<CourseModel[]>;

  constructor(
    private _store: Store,
    private _apiService: ApiService
    ) { }

  ngOnInit(): void {
    this.getAllCourses();
    this.courses$.subscribe((resp) => (this.courseList = resp));
    this.isLogin();
  }

  getAllCourses() {
    this._store.dispatch(new GetAllCourses());
  }

  isLogin() {
    const jwt = localStorage.getItem('token');
    const decodedToken = this.helper.decodeToken(jwt);
    this.isTokenExpired = this.helper.isTokenExpired(jwt);
    if(!this.isTokenExpired) {
      this.getStudiedCourses();
    }
  }

  getStudiedCourses() {
    this._apiService.getStudiedCourses().subscribe(resp=> this.studiedCourseList = resp.data);
  }
}
