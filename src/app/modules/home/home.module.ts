import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ScrollLtrDirective } from 'src/app/directives/scroll/scroll-ltr.directive';
import { ScrollRtlDirective } from 'src/app/directives/scroll/scroll-rtl.directive';

@NgModule({
  declarations: [
    HomeComponent,
    ScrollLtrDirective,
    ScrollRtlDirective
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    HomeRoutingModule,
    SharedModule
  ],
  exports: [HomeComponent],
})
export class HomeModule {}
