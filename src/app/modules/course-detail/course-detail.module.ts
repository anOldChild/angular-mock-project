import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseDetailComponent } from './course-detail.component';
import { CourseDetailRoutingModule } from './course-detail-routing.module';
import { HeaderComponent } from 'src/app/shared/header/header.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [CourseDetailComponent],
  imports: [
    CommonModule,
    CourseDetailRoutingModule,
    SharedModule,
    FontAwesomeModule,
  ],
})
export class CourseDetailModule {}
