import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import {
  faChevronUp,
  faChevronDown,
  faPlayCircle,
} from '@fortawesome/free-solid-svg-icons';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { GetCourseDetail } from 'src/app/stores/course/actions';
import { CourseDetailModel } from 'src/app/stores/course/model';
import { CourseDetailState } from 'src/app/stores/course/states';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.scss'],
})
export class CourseDetailComponent implements OnInit {
  faPlayCircle = faPlayCircle;
  courseDetail!: CourseDetailModel;
  @Select(CourseDetailState.getCourseDetailState)
  courseDetail$: Observable<CourseDetailModel>;
  constructor(private _route: ActivatedRoute, private _store: Store) {}

  ngOnInit(): void {
    this._route.params.subscribe((params: Params) => {
      this.getCourseDetail(params['hashCode']);
    });
    this.courseDetail$.subscribe((detail) => {
      this.courseDetail = detail;
    });
  }

  getCourseDetail(hashCode: string) {
    this._store.dispatch(new GetCourseDetail(hashCode));
  }
}
