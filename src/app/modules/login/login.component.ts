import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { LogInUser, Logout } from 'src/app/stores/login/actions';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoginResponseModel } from 'src/app/stores/login/models';
import { AuthState } from 'src/app/stores/login/states';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup | undefined;
  email: string | undefined;
  password: string | undefined;
  isLoginSuccess: boolean;
  isLoginFailed: boolean;

  @Select(AuthState.getTokenJwt) tokenJwt$: Observable<LoginResponseModel>;

  constructor(private _fb: FormBuilder,
              private _store: Store,
              private _authService: AuthService,
              private _router: Router
    ) { }

  ngOnInit(): void {
    localStorage.removeItem('token');
    this.isLoginSuccess = false;
    this.isLoginFailed = false;
    this._store.dispatch(new Logout());
    this.loginForm = this._fb.group({
      email: ['', [Validators.required, Validators.pattern('[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}')]],
      password: ['', [Validators.required, Validators.pattern('^[a-z0-9_-]{8,50}$')]]
    })
  }

  get f() { 
    return this.loginForm.controls; 
  };

  onSubmit() {
    let user = {username: this.loginForm.value.email, password: this.loginForm.value.password}
    this._store.dispatch(new LogInUser(user));
    this.tokenJwt$.subscribe(data => {
      if(data.statusCode && data.statusCode === 200) {
        this.isLoginFailed = false;
        this.isLoginSuccess = true;
        this._router.navigate(['']);
      }
      else if(data.statusCode !== null){
        this.isLoginSuccess = false;
        this.isLoginFailed = true;
      }
    });
  }
}
