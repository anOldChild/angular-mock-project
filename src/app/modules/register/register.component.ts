import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { ApiService } from 'src/app/services/api/api.service';

// import { AddUsers } from 'src/app/stores/register/action';
import { MustMatch } from 'src/app/stores/register/must-match.validator';
// import { RegisterUserState } from 'src/app/stores/register/states';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  // userForm:FormGroup;
  registerForm: FormGroup;
  loading = false;
  userInfo: [];
  submitted = false;
  error: string;
  // @Select(RegisterUserState.selectRegisterUserState) userInfo$: Observable<any>;
  constructor(
    private formBuilder: FormBuilder,
    private userService: ApiService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group(
      {
        FirstName: ['', Validators.required],
        Email: ['', Validators.required],
        MiddleName: [''],
        Bio: [''],
        LastName: [
          '',
          [
            Validators.required,
          ],
        ],
        PhoneNumber: ['', Validators.required],
        PasswordHash: ['', [Validators.required, Validators.minLength(6)]],
        ConfirmPassword: ['', Validators.required],
      },
      {
        validator: MustMatch('PasswordHash', 'ConfirmPassword'),
      }
    );
  }
  get f() {
    return this.registerForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    // this.store.dispatch(new AddUsers(this.registerForm.value));
    var formData: any = new FormData();
    formData.append('FirstName', this.registerForm.get('FirstName').value);
    formData.append('Email', this.registerForm.get('Email').value);
    formData.append('Bio', this.registerForm.get('Bio').value);
    formData.append('MiddleName', this.registerForm.get('MiddleName').value);
    formData.append('LastName', this.registerForm.get('LastName').value);
    formData.append('PhoneNumber', this.registerForm.get('PhoneNumber').value);
    formData.append(
      'PasswordHash',
      this.registerForm.get('PasswordHash').value
    );
    formData.append(
      'ConfirmPassword',
      this.registerForm.get('ConfirmPassword').value
    );
    this.userService
      .addUser(formData)
      .pipe(first())
      .subscribe(
        (data) => {
          this.router.navigate(['/login'], {
            queryParams: { registered: true },
          });
        },
        (errorMessage) => {
          const getErrorValue=errorMessage.error
          const errorObject = getErrorValue.errors
          for (const errorIndex in errorObject) {
            console.log(errorObject[errorIndex])
            this.error = errorIndex + ': ' + errorObject[errorIndex];
          }
          this.loading = false;
        }
      );
  }
}
