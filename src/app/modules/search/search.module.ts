import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search.component';
import { SearchRoutingModule } from './search-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";


@NgModule({
  declarations: [SearchComponent],
  imports: [CommonModule, FontAwesomeModule, SearchRoutingModule, ReactiveFormsModule, FormsModule, SharedModule,  NgbModule],
  exports: [SearchComponent],
})
export class SearchModule { }
