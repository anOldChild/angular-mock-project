import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { GetCourseDetail } from 'src/app/stores/course/actions';
import { CourseDetailModel } from 'src/app/stores/course/model';
import { CourseDetailState } from 'src/app/stores/course/states';
import { CourseModel } from 'src/app/stores/home/models';
import { GetCourseSearch } from 'src/app/stores/search/actions';
import { CourseResponseSearchModel } from 'src/app/stores/search/models';
import { CourseSearchState } from 'src/app/stores/search/states';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  faChevronLeft = faChevronLeft;
  faChevronRight = faChevronRight;
  page: number= 1;
  toggle: boolean = false;
  nameCourseSearch: string;
  levelCourse!: string;
  sortYear!: string;
  courseList!: CourseModel[];
  list!: any[];
  queryParamsRoot: Params;
  @Select(CourseSearchState.getCourseSearchState)
  courseSearch$: Observable<CourseResponseSearchModel>;
  constructor(private _route: ActivatedRoute, private _store: Store, private _api: ApiService, private _router: Router) { }
  ngOnInit(): void {
    this.queryParamsRoot = {...this._route.snapshot.queryParams };
    this._route.queryParams.subscribe((queryParams: any) => {
      this.nameCourseSearch = queryParams['nameCourse'];
      queryParams = Object.keys(queryParams).map(key => {
        return `${key}=${encodeURIComponent(queryParams[key])}`;
      })
      this.getSearchCourse(queryParams.join('&'));
    })
    this.courseSearch$.subscribe((res: any) => {
      this.courseList = res.data;

    })
  }
  getSearchCourse(queryParams: string) {
    this._store.dispatch(new GetCourseSearch(queryParams));
  }

  toggleSidebar() {
    this.toggle = !this.toggle;
  }

  addFillter() {
    let queryParams: Params;
    if (this.levelCourse || this.sortYear !== 'undefined') {
      queryParams = { ...this.queryParamsRoot, ...{ levelCourse: this.levelCourse }, ...{ sortYear: this.sortYear }, ...{page: this.page} }
    } else {
      queryParams = { ...this.queryParamsRoot, ...{page: this.page} }
    }
    this._router.navigate([], { queryParams: queryParams})
  }

  clearFilter() {
    this.levelCourse = undefined;
    this.sortYear = undefined;
    this.addFillter();
  }
}
