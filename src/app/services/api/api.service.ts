import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CourseResponseModel } from '../../stores/course/model';
import { CoursesModel } from '../../stores/home/models';
import { CourseResponseSearchModel } from '../../stores/search/models';
import { RegisterUserModel } from '../../stores/register/models';


@Injectable({
  providedIn: 'root',
})
export class ApiService {
  ALL_COURSES_API =
    'https://localhost:5001/api/1.0/Course/get-all-course?page=1';
  STUDIED_COURSES_API =
    'https://localhost:5001/api/1.0/Course/get-studied-course?hashCodeUser=da8bff38-f70a-47e7-af92-ea8d7bce8b7b&page=1';
  COURSE_BY_HASHCODE_API =
    'https://localhost:5001/api/1.0/Course/get-course-by-hashcode?hashCode=26f7c698-4097-45af-be9a-6439c913cd89';

  // [Author]: Dang Kieu - Post Account API
  REGISTER_USER = 'https://localhost:5001/api/1.0/Authentication/register';

  constructor(private _http: HttpClient) {}

  getAllcourses() {
    return this._http.get<CoursesModel>(this.ALL_COURSES_API);
  }
  // call api to return a fake data 
  getStudiedCourses() {
    return this._http.get<CoursesModel>(
      'https://localhost:5001/api/1.0/Course/get-all-course?page=2'
    );
  }
  getCourseDetailByHashCode(hashCode: string) {
    return this._http.get<CourseResponseModel>(
      `https://localhost:5001/api/1.0/Course/get-course-by-hashcode?courseHashCode=${hashCode}`
    );
  }
  // [Author]: Dang Kieu - Call API - Register
  addUser(userData) {
    console.log(userData);
    return this._http.post<RegisterUserModel>(this.REGISTER_USER, userData);
  }
  getSearchCourse(queryParams: string): Observable<CourseResponseSearchModel> {
    return this._http.get<CourseResponseSearchModel>(
      `https://localhost:5001/api/1.0/Course/search-course?${queryParams}`
    );
  }
}
