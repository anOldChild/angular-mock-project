import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginResponseModel } from '../../stores/login/models';
import { JwtHelperService } from '@auth0/angular-jwt';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  LOGIN_API = 'https://localhost:5001/api/1.0/Authentication/login';
  helper = new JwtHelperService();

  constructor(private _http: HttpClient) { }

  login(user) {
    return this._http.post<LoginResponseModel>(this.LOGIN_API, user).pipe(
      map(response => {
        localStorage.removeItem('token');
        localStorage.setItem('token', response.data);
        return response;
      })
    );
  }

  loggedIn():boolean {
    const token = localStorage.getItem('token');
    return !this.helper.isTokenExpired(token);
  }
}  
