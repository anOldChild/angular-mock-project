import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { faBars, faHiking, faHome, faLightbulb, faNewspaper, faSearch, faShoppingCart, faSignInAlt } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  faBars = faBars;
  faSignInAlt = faSignInAlt;
  faShoppingCart = faShoppingCart;
  faSearch = faSearch;
  faNewspaper = faNewspaper;
  faHiking = faHiking;
  faLightbulb = faLightbulb;
  faHome = faHome;


  @Input() nameCourseSearch!: string;
  formSearch!: FormGroup;
  helper = new JwtHelperService();
  isTokenExpired: boolean = false;
  isLoggedIn: boolean = false;
  avatarLink: string = 'https://localhost:5001/avatars/';
  isDisplayed: boolean = false;
  username: string = '';
  constructor(private router: Router) {
  }
  ngOnInit(): void {
    this.formSearch = new FormGroup({
      search: new FormControl(this.nameCourseSearch, [Validators.required])
    });
    this.checkLogin();
  }

  checkLogin() {
    const jwt = localStorage.getItem('token');
    const decodedToken = this.helper.decodeToken(jwt);
    this.isTokenExpired = this.helper.isTokenExpired(jwt);
    if(!this.isTokenExpired) {
      this.username = decodedToken.FullName;
      this.avatarLink = this.avatarLink+=decodedToken.Avatar;
      this.isLoggedIn = !this.isLoggedIn;
    }
  }
  toggle() {
    this.isDisplayed = !this.isDisplayed;
  }
  onSubmit() {
    if (this.formSearch.invalid) return;
    this.router.navigate(['/course/search-course'], { queryParams: { nameCourse: this.formSearch.value.search, page: 1 } });
  }
}
