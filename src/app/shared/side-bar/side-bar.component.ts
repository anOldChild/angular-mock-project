import { Component, OnInit } from '@angular/core';
import { faHiking, faHome, faLightbulb, faNewspaper, faPen, faPlus, faQuestion, faShare } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  faHome = faHome;
  faHiking = faHiking;
  faLightbulb = faLightbulb;
  faNewspaper = faNewspaper;
  faPen = faPen;
  faQuestion = faQuestion;
  faShare = faShare;
  faPlus = faPlus;
  constructor() { }

  ngOnInit(): void {
  }

}
