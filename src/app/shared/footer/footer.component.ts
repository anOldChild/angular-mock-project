import { Component, OnInit } from '@angular/core';
import { faChevronUp, faThumbsDown, faThumbsUp } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  faThumbsDown = faThumbsDown;
  faThumbsUp = faThumbsUp;
  faChevronUp = faChevronUp;
  constructor() { }

  ngOnInit(): void {
  }

}
