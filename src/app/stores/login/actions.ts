import { LoginResponseModel, UserModel } from "./models";

export class LogInUser {
  static readonly type = '[Auth] Log In User';
  constructor(public readonly payload: UserModel) {}
}

export class LogInUserSuccess {
  static readonly type = '[Auth] Log In User Success';
  constructor(public readonly payload: LoginResponseModel) {}
}

export class LogInUserFail {
  static readonly type = '[Auth] Log In User Fail';
  constructor(public readonly payload: LoginResponseModel) {}
}

export class Logout {
  static readonly type = '[Auth] Logout';
}

export class LogoutSuccess {
  static type = '[Auth] LogoutSuccess';
}




