import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { catchError, tap } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LogInUser, LogInUserFail, LogInUserSuccess, Logout, LogoutSuccess } from './actions';
import { LoginResponseModel, UserModel } from './models';

@Injectable({
    providedIn: 'root',
  })
@State<LoginResponseModel>({
    name: 'auth',
    defaults: {}
})
export class AuthState {
    constructor(private _authService: AuthService,
                private _router : Router
        ) {}
    @Action(LogInUser)
    loginUser({ dispatch }: StateContext<LoginResponseModel>,
              { payload }: LogInUser
        ) {
        return this._authService.login(payload).pipe(
            tap((user) => {
                return dispatch(new LogInUserSuccess(user));
            }),
            catchError((err) => {
                return dispatch(new LogInUserFail(err.error));
              })
        )
    }

    @Action(LogInUserSuccess)
    logInUserSuccess({ setState }: StateContext<LoginResponseModel>,
                    { payload }: LogInUserSuccess
        ) {
        setState(payload);
    }

    @Action(LogInUserFail)
    logInUserFail({ setState }: StateContext<LoginResponseModel>,
                    { payload }: LogInUserFail) {
        setState({});
    }

    @Action(Logout)
    logout(ctx: StateContext<LoginResponseModel>) {
      return ctx.dispatch(new LogoutSuccess());
  }

  @Action(LogoutSuccess)
  logoutSuccess(ctx: StateContext<LoginResponseModel>) {
    ctx.patchState({
      data: null,
      message: null,
      statusCode: null
    });
  }

    @Selector()
    static getTokenJwt(res: LoginResponseModel) {
        return res;
      }
}