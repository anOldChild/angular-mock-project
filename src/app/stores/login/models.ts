export interface LoginResponseModel {
    data?: string;
    message?: string | null;
    statusCode?: number;
  }

export interface UserModel {
  username?: string;
  password?: string;
}