// import { Injectable } from '@angular/core';
// import { loggerOptionsFactory } from '@ngxs/logger-plugin/src/logger.module';
// import { Action, Selector, State, StateContext } from '@ngxs/store';
// import { catchError, tap } from 'rxjs/operators';
// import { ApiService } from 'src/app/services/api.service';
// import { AddUsers, AddUsersFail, AddUsersSuccess } from './action';
// import { RegisterUserModel } from './models';

// @Injectable()
// @State<RegisterUserModel>({
//   name: 'addUsers',
//   defaults: {
      
//   },
// })
// export class RegisterUserState {
//   constructor(private _apiService: ApiService) {}

//   @Action(AddUsers)
//   AddUsers(
//     { dispatch, patchState }: StateContext<RegisterUserModel>,
//     { payload }: AddUsers
//   ) {
//     return this._apiService.addUser(payload).pipe(
//       tap((res) => {
//         if (res) {
//             patchState(res);
//             dispatch(new AddUsersSuccess(res));
//         } else {
//             dispatch(new AddUsersFail('Add user not being loaded..'));
//         }
//       }),
//       catchError((err) => err)
//     );
//   }

//   @Selector()
//   static selectRegisterUserState(addUsers: RegisterUserModel) {
//     return addUsers;
//   }
// }
