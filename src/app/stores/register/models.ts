
export interface RegisterUserModel {
    avatar?: string;
    Email?: string;
    FirstName?: string;
    MiddleName?: string;
    LastName?: string;
    PhoneNumber?: string;
    PasswordHash?: string;
    ConfirmPassword?: string;
    Bio?: string
  }