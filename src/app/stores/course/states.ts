import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { catchError, tap } from 'rxjs/operators';
import { ApiService } from 'src/app/services/api/api.service';
import {
  GetCourseDetail,
  GetCourseDetailFail,
  GetCourseDetailSuccess,
} from './actions';
import { CourseDetailModel, CourseResponseModel } from './model';

@Injectable()
@State<CourseResponseModel>({
  name: 'courseDetail',
  defaults: {},
})
export class CourseDetailState {
  constructor(private _apiService: ApiService) { }

  @Action(GetCourseDetail)
  getCourseDetail(
    { dispatch, patchState }: StateContext<CourseDetailModel>,
    { payload }: GetCourseDetail
  ) {
    return this._apiService.getCourseDetailByHashCode(payload).pipe(
      tap((res) => {
        if (res) {
          patchState(res.data);
          dispatch(new GetCourseDetailSuccess(res));
        } else {
          dispatch(new GetCourseDetailFail('Course Detail not being loaded..'));
        }
      }),
      catchError((err) => err)
    );
  }

  @Selector()
  static getCourseDetailState(courseDetail: CourseDetailModel) {
    return courseDetail;
  }
}
