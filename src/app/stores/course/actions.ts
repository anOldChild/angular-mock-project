import { CourseResponseModel } from './model';

export class GetCourseDetail {
  static readonly type = '[Course Detail] Get Course Detail';
  constructor(public payload: string) { }
}

export class GetCourseDetailSuccess {
  static readonly type = '[Course Detail] Get Course Detail Successfull';
  constructor(public payload: CourseResponseModel) { }
}

export class GetCourseDetailFail {
  static readonly type = '[Course Detail] Get Course Detail Failed';
  constructor(public payload: string) { }
}

