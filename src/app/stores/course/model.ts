export interface SectionModel {
  title?: string;
  description?: string;
  courseHashCode?: string;
  priority?: number;
}

export interface CourseDetailModel {
  title?: string;
  image?: string;
  totalStudent?: number;
  description?: string;
  totalLession?: number;
  totalTime?: string;
  level?: number;
  sections?: SectionModel[];
  descriptionDetails?: string[];
  hasdCode?: string;
}

export interface CourseResponseModel {
  data?: CourseDetailModel;
  message?: string | null;
  statusCode?: number;
}
