import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { catchError, tap } from 'rxjs/operators';
import { ApiService } from 'src/app/services/api/api.service';
import { CoursesModel } from './models';
import {
  GetAllCourses,
  GetCoursesFailed,
  GetCoursesSuccess
} from './actions';

@Injectable()
@State<CoursesModel>({
  name: 'courses',
  defaults: {},
})
export class CourseState {
  constructor(private _apiService: ApiService) { }
  @Action(GetAllCourses, { cancelUncompleted: true })
  getAllCourses({ dispatch }: StateContext<CoursesModel>) {
    return this._apiService.getAllcourses().pipe(
      tap((courses) => {
        if (courses) {
          dispatch(new GetCoursesSuccess(courses));
        } else {
          dispatch(new GetCoursesFailed());
        }
      }),
      catchError((err) => err)
    );
  }

  @Action(GetCoursesSuccess)
  getCoursesSuccess(
    { setState }: StateContext<CoursesModel>,
    { payload }: GetCoursesSuccess
  ) {
    setState(payload);
  }

  @Selector()
  static getCourseState(courses: CoursesModel) {
    return courses.data;
  }
}
