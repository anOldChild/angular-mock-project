import { CoursesModel } from "./models";

export class GetCoursesSuccess {
  static readonly type = '[Home] Get Courses Successfully';
  constructor(public payload: CoursesModel) { }
}

export class GetAllCourses {
  static readonly type = '[Home] Get All Courses';
}

export class GetCoursesFailed {
  static readonly type = '[Home] Get Courses Failed ';
}
