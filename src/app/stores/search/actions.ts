export class GetCourseSearch {
    static readonly type = '[Course Search] get course search';
    constructor(public payload: string) {
    }
}

export class GetCourseSearchFail {
    static readonly type = '[Course Search Fail] Get Course Search Failed';
    constructor(public payload: string) { }
}