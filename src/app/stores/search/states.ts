import { Injectable } from '@angular/core';
import { Action, Actions, Selector, State, StateContext } from '@ngxs/store';
import { of } from 'rxjs';

import { catchError, tap } from 'rxjs/operators';
import { ApiService } from 'src/app/services/api/api.service';
import { GetCourseSearch, GetCourseSearchFail } from './actions';
import { CourseResponseSearchModel } from './models';

@State<CourseResponseSearchModel>({
    name: 'courseSearch',
    defaults: {},
})
@Injectable()
export class CourseSearchState {
    constructor(private _apiService: ApiService) { }
    @Action(GetCourseSearch)
    getCourseSearch(
        { dispatch, patchState }: StateContext<CourseResponseSearchModel>,
        { payload }: GetCourseSearch
    ) {
        return this._apiService.getSearchCourse(payload).pipe(
            tap((res) => {
                if (res.data) {
                    patchState(res);
                }
            }),
            catchError(error => {
                patchState({ data: [] });
                return of(false);
            })
        )
    }
    @Selector()
    static getCourseSearchState(courseResponseModel: CourseResponseSearchModel) {
        return courseResponseModel;
    }

}


