
export interface CourseSearch {
    title?: string;
    image?: string;
    totalStudent?: number;
    hashCode?: string;
}
export interface CourseResponseSearchModel {
    data?: CourseSearch[];
    message?: string | null;
    statusCode?: number
}