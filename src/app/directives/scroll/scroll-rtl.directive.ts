import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appScrollRtl]'
})
export class ScrollRtlDirective {

  constructor(private _el: ElementRef) { }
  @HostListener('click') moveToLeft() {
    console.log(1);
    
    const container = this._el.nativeElement.parentElement.nextElementSibling;
    container.scrollLeft -= 320;
  }
}
