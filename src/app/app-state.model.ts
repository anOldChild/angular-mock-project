import { CourseDetailState } from './stores/course/states';
import { CourseState } from './stores/home/states';
// import { RegisterUserState } from './stores/register/states'; RegisterUserState
import { AuthState } from './stores/login/states';
import { CourseSearchState } from './stores/search/states';

export const appState = [
  CourseState,
  CourseDetailState,
  CourseSearchState,
  AuthState,
];
